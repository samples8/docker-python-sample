#!/bin/bash
if [ -n "${CI_REGISTRY}" ] ; then # we are running on gitlab CI/CD
    docker run --rm ${CI_REGISTRY_IMAGE}
else    # local testing
    docker run --rm assigment1
fi