#!/bin/bash
if [ -n "${CI_REGISTRY}" ] ; then # we are running on gitlab CI/CD
    docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} 
    docker build -t ${CI_REGISTRY_IMAGE} docker
    docker push ${CI_REGISTRY_IMAGE}
else # local testing
    docker build -t assigment1 docker
fi