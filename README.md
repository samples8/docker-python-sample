Python 3.x script called ‘HoeWarmIsHetInDelft.py’ that retrieves from the Internet the current temperature in Delft and prints it to standard output, rounded to degrees Celsius. Obtain the information from http://www.weerindelft.nl/
>
>User types:
>
>python HoeWarmIsHetInDelft.py
>
>Response:
>
>18 degrees Celsius
>
>2 | Package the code above in a docker container
>3 | gitlab-ci.yml pipeline  that installs the container above and  executes the script


Requestor should specify where code should be deployed


Code execution
==========================
## Local
Prerequisites:
* installed git
* installed docker engine

```bash
./build.sh
./run.sh
```

Expected result
```

robert@ice /tmp/docker-python-sample $ ./build.sh
Sending build context to Docker daemon  6.144kB
Step 1/7 : FROM python:3.8.1-alpine3.11
 ---> a0ee0c90a0db
Step 2/7 : ADD app /app
 ---> 543b4339d017
Step 3/7 : RUN apk update
 ---> Running in 713db0d70b03
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
v3.11.3-31-g2e6d6d513d [http://dl-cdn.alpinelinux.org/alpine/v3.11/main]
v3.11.3-30-g2d9c1a116c [http://dl-cdn.alpinelinux.org/alpine/v3.11/community]
OK: 11259 distinct packages available
Removing intermediate container 713db0d70b03
 ---> 215f84045e79
Step 4/7 : RUN apk add py3-virtualenv
 ---> Running in 362223bd9d6b
(1/2) Installing python3 (3.8.1-r0)
(2/2) Installing py3-virtualenv (16.7.8-r0)
Executing busybox-1.31.1-r9.trigger
OK: 78 MiB in 37 packages
Removing intermediate container 362223bd9d6b
 ---> 204da73c22ec
Step 5/7 : WORKDIR /app
 ---> Running in bb457653da7d
Removing intermediate container bb457653da7d
 ---> 775fcff5bd6c
Step 6/7 : RUN ./setup.sh
 ---> Running in 8ccbbe63d14a
Already using interpreter /usr/local/bin/python3
Using base prefix '/usr/local'
New python executable in /app/.env/bin/python3
Also creating executable in /app/.env/bin/python
Installing setuptools, pip, wheel...
done.
Running virtualenv with interpreter /usr/local/bin/python3
Collecting requests
  Downloading requests-2.22.0-py2.py3-none-any.whl (57 kB)
Collecting urllib3!=1.25.0,!=1.25.1,<1.26,>=1.21.1
  Downloading urllib3-1.25.8-py2.py3-none-any.whl (125 kB)
Collecting certifi>=2017.4.17
  Downloading certifi-2019.11.28-py2.py3-none-any.whl (156 kB)
Collecting chardet<3.1.0,>=3.0.2
  Downloading chardet-3.0.4-py2.py3-none-any.whl (133 kB)
Collecting idna<2.9,>=2.5
  Downloading idna-2.8-py2.py3-none-any.whl (58 kB)
Installing collected packages: urllib3, certifi, chardet, idna, requests
Successfully installed certifi-2019.11.28 chardet-3.0.4 idna-2.8 requests-2.22.0 urllib3-1.25.8
Removing intermediate container 8ccbbe63d14a
 ---> 66c076baac91
Step 7/7 : CMD .env/bin/python HoeWarmIsHetInDelft.py
 ---> Running in 8ebb10dfe9a7
Removing intermediate container 8ebb10dfe9a7
 ---> 8ac5f78f731b
Successfully built 8ac5f78f731b
Successfully tagged docker-python-sample:latest
robert@ice /tmp/docker-python-sample $ ./run.sh
12 degrees Celsius
```
## Execution in GitLab CI/CD pipeline 


Pipeline
* (test) Performs code quality check in SonarCloud
* (build) Builds docker image and pushes is to private project registry
* (run) Pulls docker image and runs the code with requested result