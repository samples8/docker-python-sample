#!/bin/sh

virtualenv -p python3 .env
. .env/bin/activate
pip install -U -r setup/requirements.txt